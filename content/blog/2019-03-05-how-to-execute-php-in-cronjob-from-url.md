+++
title = "How to Execute PHP in Cronjob From Url"
date = 2019-03-05T15:00:50+07:00
tags = ["cronjob","url","curl"]
categories = ["php"]
+++

### Method 1 - Curl

```shh
0 10 * * * curl -s "http://www.domain.com/index.php" > /dev/null
0 10 * * * curl -s "url 2..." > /dev/null
```

This will run curl, fetching only the URL's. It'll ignore redirects and all media on page. It'll be executed at 10:00.

You can add entries to crontab using the command `crontab -e`.

If curl is not installed, you can install it with `sudo apt-get install curl`

Curl is also highly customizable and scriptable. man curl will give you a overview of this. In the above example `-s` is used to make **_curl silent_**, so no output is produced.

You can also add the curl commands in a file, like this:

```ssh
#!/bin/bash
curl -s "url 1" > /dev/null
curl -s "url 2" > /dev/null
```

save the file as for instance `/home/username/bin/curlscript.sh`, and run `chmod +x curlscript.sh`. This can then be used in cron, instead of listing each command.

### Method 2 - lynx

lynx is terminal based web browser on flavors of Linux. Lynx displays plain ASCII text on your terminal and does not displays any images or multimedia content. Install it by typing

```ssh
sudo apt-get install lynx
```

To use it on cronjob

```shh
0 10 * * * lynx "http://www.domain.com/index.php"
0 10 * * * lynx "url 2..."
```

### Method 3 - php-cli

This is not from url but run php script in your cron.

If `php-cli` is not installed, you can install it with `sudo apt-get install php-cli`.

```shh
0 10 * * * php /home/username/directoryname/phpscript.php
```

Instead of using url, you can us php file path

Note:

*if you use `php-cli` make sure your use `__DIR__` when include another file.*

[Source](https://askubuntu.com/a/959336)
