+++
title = "Beginner First Mistakes"
date = 2019-01-24T12:41:45+07:00
tags = [
    "go",
    "golang",
    "templates",
    "themes",
    "development",
]
categories = [
    "hugo",
    "golang",
]
+++

### This is my first mistake during learning hugo.

My first intention while learning hugo is to make this blog up and host in github. 

According to official gohugo.io tutorial [hosting on github pages](https://gohugo.io/hosting-and-deployment/hosting-on-github/), you should completely remove the `public` directory by typing `rm -rf public`.

The problem is there is no public directory in my hugo project. So, I did some browsing and found out that I did not completely read `docs`. Instead type `hugo server`, I should type `hugo` then `public` directory come up in my parent project folder.

Then, to write destination gererated file, add `-d` flag `hugo -d ../your/path/`

That's it.