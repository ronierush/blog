+++
title = "How to Create tar.gz File in Windows Using 7-Zip"
date = 2019-03-07T14:01:49+07:00
tags = ["compress","7zip","extract"]
categories = ["windows"]
+++

### SETUP ENVIRONMENT VARIABLES

Before we begin, download 7-Zip [here](https://www.7-zip.org/download.html), then install to your computer.

1. Right click `Computer or This PC (windows 10)`
2. Select `Properties`
3. On left sidebar click `Advanced system settings`
4. At the bottom click `Environment Variables`
5. Under user variables, select `PATH` then edit
6. Add `C:\Program Files\7-Zip` at variable value, if you using windows 7 or bellow add `C:\Program Files\7-Zip;` at the begining of line. Don't forget the semicolon!
7. Click Ok and Ok

### 7-Zip Command Line

First open command line by typing `windows + r` then check 7-Zip by typing `7z` in your command line to make sure it's work.

###### Create .tar.gz:

```ssh
7z a -ttar -so filename.tar foldername/ | 7z a -si filename.tar.gz
```

###### Extract .tar.gz:

```ssh
7z x filename.tar.gz -so | 7z x -si -ttar
```

## LINUX

###### Create .tar.gz:

```ssh
tar -zcvf filename.tar.gz foldername/
```

###### Extract .tar.gz:

```ssh
tar -zxvf filename.tar.gz -C foldername/
```

That's it.
