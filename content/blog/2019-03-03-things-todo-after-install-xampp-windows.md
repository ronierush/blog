+++
title = "Things Todo After Install XAMPP Windows"
date = 2019-03-03T14:25:10+07:00
draft = false
tags = [
	"xampp",
	"mysql",
	"php"
]
categories = ["windows"]
+++
#### Change htdocs location

file `C:\xampp\apache\conf\httpd.conf`

```ssh
DocumentRoot "E:/xampp/htdocs"
<Directory "E:/xampp/htdocs">
```

#### Change mysql data location

file `C:\xampp\mysql\bin\my.ini` 

```ssh
datadir = "E:/xampp/mysql/data"
innodb_data_home_dir = "E:/xampp/mysql/data"
innodb_log_group_home_dir = "E:/xampp/mysql/data"
```

#### increase upload size, memory, and execution time

file `C:\xampp\php\php.ini`

```ssh
post_max_size = 128M
upload_max_filesize = 128M 
max_execution_time = 2000
max_input_time = 3000
memory_limit = 256M
```