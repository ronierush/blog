---
title: "Hugolib: Extract Date and Slug From Filename"
date: 2019-04-17T08:18:17+07:00
tags: ["hugo"]
---

This commit makes it possible to extract the date from the content filename. Also, the filenames in these cases will make for very poor permalinks, so we will also use the remaining part as the page `slug` if that value is not set in front matter.

This should make it easier to move content from Jekyll to Hugo.

To enable, put this in your `config.toml`:

```json
[frontmatter]
  date  = [":filename", ":default"]
```

This commit is also a spring cleaning of how the different dates are configured in Hugo. Hugo will check for dates following the configuration from left to right, starting with `:filename` etc.

So, if you want to use the `file modification time`, this can be a good configuration:

```json
[frontmatter]
  date = [ "date",":fileModTime", ":default"]
  lastmod = ["lastmod" ,":fileModTime", ":default"]
```

The current `:default` values for the different dates are

```json
[frontmatter]
date = ["date","publishDate", "lastmod"]
lastmod = ["lastmod", "date","publishDate"]
publishDate = ["publishDate", "date"]
expiryDate = ["expiryDate"]
```

The above will now be the same as:

```json
[frontmatter]
date = [":default"]
lastmod = [":default"]
publishDate = [":default"]
expiryDate = [":default"]
```

An example of a custom configuration using a custom date front matter field:

```json
[frontmatter]
date = [ "myDate", ":default"]
```

Note:

- We have some built-in aliases to the above: lastmod => modified, publishDate => pubdate, published and expiryDate => unpublishdate.
- If you want a new configuration for, say, `date`, you can provide only that line, and the rest will be preserved.
- All the keywords to the right that does not start with a ":" maps to front matter parameters, and can be any date param (e.g. `myCustomDateParam`).
- The keywords to the left are the **4 predefined dates in Hugo**, i.e. they are constant values.
- The current "special date handlers" are `:fileModTime` and `:filename`. We will soon add :git to that list.

