+++
title = "Django - Set Up Environment"
date = 2019-03-06T18:56:11+07:00
tags = ["pipenv","django","learn"]
categories = ["django","python"]
+++

#### Basic Requirement

- Python 3.x.x
- Pip3

#### Steps

Install virtual Environment

```ssh
pip3 install pipenv
```

Create project folder. 

Because using virtual Environment, instead project-name as folder name, I prefer using django with version as project folder name. So, I can put another project inside if using the same django version.

Current latest version = `2.1.7`

```ssh
mkdir django-v2.1.7
```

Go to folder

```ssh
cd django-v2.1.7
```

Use `pipenv` to install django 

```ssh
pipenv install django==2.1.7
```

Once you type in and press enter. This should create virtual Environment for you with django installed

The reason why you need to create virtual Environment using `pipenv`. It's because the virtual Environment specify which version of django you want to use.

So, if you have 2 projects with different django version. You'll be able to create 2 sparate virtual Environment 1 for each.

Let say you want to crate Django 2.1 for Project A and Django 2.2 for project B.

This is clearly what virtual Environment for.

To make sure the the `pipenv` work you can type `ls`, and you will see `Pipfile` and `Pipfile.lock`

Now, let's go to virtual enirontment by typing in

```shh
pipenv shell
```

Once you in virtual Environment. Create django project 

```ssh
django-admin startproject django101 ./
```

Now let's run the django

```ssh
python manage.py runserver
```

Congratutlation you just run django

Note:

if you already exit from project and want to run again, start with `pipenv shell` step.