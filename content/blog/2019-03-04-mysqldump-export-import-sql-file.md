+++
title = "Mysqldump Export Import Sql File"
date = 2019-03-04T10:00:53+07:00
tags = [
	"mysqldump",
	"mysql-cli"
]
categories = ["mysql"]
+++

### Import

Type the following command to import sql data file:

```ssh
$ mysql -u username -p -h localhost DATA-BASE-NAME < data.sql
```

In this example, import `data.sql` file into `blog` database using `ronie` as username:

```ssh
$ mysql -u ronie -p -h localhost blog < data.sql
```

If you have a dedicated database server, replace localhost hostname with with actual server name or IP address as follows:

```ssh
$ mysql -u username -p -h 202.54.1.10 databasename < data.sql
```

### Export

in order to dump a set of one or more tables

```ssh
mysqldump -u username -p databasename tablename > filename.sql
```

To export a set of complete database, use the following:

```ssh
mysqldump -u username -p databasename > filename.sql
```

Note the `<` and `>` symbols in each case.
